# Compiling documentation with SassDoc

[https://github.com/SassDoc/sassdoc](https://github.com/SassDoc/sassdoc)

1. Install the Node package `sassdoc`

    $ npm install sassdoc

2. Run `sassdoc ../stylesheets ../docs/sass` from this directory
