# DocGen: Hologram & Sassdoc Launchpad

## Requirements

* [Ruby](https://www.ruby-lang.org/en/installation/)
* [Node.js](http://nodejs.org/)

## Getting Started

1. Clone this directory in your project folder:

        git clone https://github.com/docgen/docgen _docgen

1. Create the following directory structure next to this one:

        stylesheets
        └── scss

The `css` and `_docs` directory will be created by Grunt/Sassdoc/Hologram, resulting in this directory structure:

        _docgen
        stylesheets
        ├── _docs
        ├── css
        └── scss

1. If necessary, modify `Gruntfile.js`'s Sass options to point to your main `.scss` file in `stylesheets/scss`.

1. Run `npm install` in the `_docgen` directory to get all the necessary Node.js packages.

## References

* Hologram: [https://github.com/trulia/hologram](https://github.com/trulia/hologram)
* SassDoc: [https://github.com/SassDoc/sassdoc](https://github.com/SassDoc/sassdoc)
