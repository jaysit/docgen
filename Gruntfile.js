/*global module:false*/
module.exports = function(grunt) {

  // Project configuration.
  grunt.initConfig({
	// Metadata.
	meta: {
	  version: '0.1.0'
	},
	watch: {
		gruntfile: {
			files: [
				'../stylesheets/scss/**/*.scss',
			],
			tasks: ['sass', 'hologram', 'sassdoc', 'autoprefixer']
		},
		hologram: {
			files: [
				'../stylesheets/scss/_hologram-docs/**/*.md',
				'_hologram/doc_assets/**/*.html'
			],
			tasks: ['hologram']
		}
	},
	autoprefixer: {
		dist: {
			files: {
				'../stylesheets/css/mit-branding.css': '../stylesheets/css/mit-branding-unprefixed.css',
				'../stylesheets/css/mit-branding.min.css': '../stylesheets/css/mit-branding-unprefixed.min.css'
			}
		}
	},
	sass: {
		expanded: {
			options: {
				outputStyle: 'expanded',
				sourcemap: true
			},
			files: {
				'../stylesheets/css/mit-branding-unprefixed.css': '../stylesheets/scss/mit-branding.css.scss'
			}
		},
		compressed: {
			options: {
				outputStyle: 'compressed',
				sourcemap: true
			},
			files: {
				'../stylesheets/css/mit-branding-unprefixed.min.css': '../stylesheets/scss/mit-branding.css.scss'
			}
		},
	},
	hologram: {
		generate: {
			options: {
				config: '_hologram/hologram_config.yml'
			}
		}
	},
	sassdoc: {
		src: '../stylesheets/scss/**/*.scss',
		options: {
			dest: '../stylesheets/_docs/sass',
			package: './_sassdoc/package.json',
			display: {
				access: ["public"],
				alias: false,
				watermark: true
			},
			groups: {
				branding: "Branding",
				buttons: "Buttons",
				undefined: "General",
				logos: "Logos",
				fonts: "Fonts",
				colors: "Colors"
			},
			exclude: [
				"vendor/**",
			],
		}
	},
	});

	// These plugins provide necessary tasks.
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-sass');
	grunt.loadNpmTasks('grunt-hologram');
	grunt.loadNpmTasks('grunt-sassdoc');
	grunt.loadNpmTasks('grunt-autoprefixer');

	// Default task.
	grunt.registerTask('default', ['sass', 'sassdoc', 'hologram', 'watch', 'autoprefixer']);

};
