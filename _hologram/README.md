# Compiling documentation with Hologram

[http://trulia.github.io/hologram/](http://trulia.github.io/hologram/)

1. Install the Ruby gem Hologram.

    $ gem install hologram

2. Run `hologram` from this directory.
